#!/bin/sh
source ./common.sh
echo "Removing $ZIP_DIR/$PLUGIN_NAME.zip"
rm -f "$ZIP_DIR/$PLUGIN_NAME.zip"
echo "Making zip for $PLUGIN_NAME"
zip -r "$ZIP_DIR/$PLUGIN_NAME.zip" $PLUGIN_NAME -x@exclude.lst

# Copy to live server?
#
echo
read -p "Publish to live server? " -n 1 -r
if [[ "$REPLY" =~ ^[Yy]$ ]]
then

    # Translate to SLP4 zip name
    #
    LIVE_NAME=$PLUGIN_NAME
    case "$PLUGIN_NAME" in
        'store-locator-le' )
            LIVE_NAME='slp4' ;;
        'slp-enhanced-map' )
            LIVE_NAME='slp4-em' ;;
        'slp-enhanced-results' )
            LIVE_NAME='slp4-er' ;;
        'slp-enhanced-search' )
            LIVE_NAME='slp4-es' ;;
        'slp-pages' )
            LIVE_NAME='slp4-pages' ;;
        'slp-pro' )
            LIVE_NAME='slp4-pro' ;;
        'slp-tagalong' )
            LIVE_NAME='slp4-tagalong' ;;
        'slp-user-managed-locations' )
            LIVE_NAME='slp4-user-managed-locations' ;;
        'slp-widgets' )
            LIVE_NAME='slp4-widgets' ;;
    esac

    echo
    echo "Publishing $PLUGIN_NAME to $LIVE_NAME on live server..."
    cp ~/myplugins/$PLUGIN_NAME.zip /home/lcleveland/NetBeansProjects/csa_licman/$LIVE_NAME.zip
fi
echo

